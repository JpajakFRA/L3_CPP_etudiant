#include "produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, Produit_test1)  {
    Produit prod(5,"describe");
    CHECK_EQUAL(5,prod.getId());
    CHECK_EQUAL("describe",prod.getDescription());
}
