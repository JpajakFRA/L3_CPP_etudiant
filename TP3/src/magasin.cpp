#include "magasin.hpp"
#include <iostream>
#include <algorithm>
#include <utility>

Magasin::Magasin()
{
    _idCourantClient=0;
    _idCourantProduit=0;
}

int Magasin::nbClients() const {
    return _clients.size();
}

void Magasin::ajouterClient(const std::string & nom){
    _clients.push_back(Client(_idCourantClient,nom));
    _idCourantClient++;
}

void Magasin:: afficherClients() const{
    std::cout<<"Liste des clients du magasin :"<<std::endl;
    for(const Client & c:_clients)
    {
        c.afficherClient();
    }
}

void Magasin:: supprimerClient(int idClient){
    //if(!idClient)
    //   throw std::string("erreur: ce client n'existe pas");
    int pos=0;
    while(idClient != _clients[pos].getId())
    {
        pos++;
    }

    std::swap(_clients[pos],_clients[_clients.size()-1]);
    _clients.pop_back();
}

int Magasin::nbProduits() const
{
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string & nom){
    _produits.push_back(Produit(_idCourantProduit,nom));
    _idCourantProduit++;
}

void Magasin:: afficherProduits() const{
    std::cout<<"Liste des produits du magasin :"<<std::endl;
    for(const Produit & p:_produits)
    {
        p.afficherProduit();
    }
}

void Magasin:: supprimerProduit(int idProduit){
    //if(!idProduit)
    //    throw std::string("erreur: ce produit n'existe pas");
    int pos=0;
    while(idProduit != _produits[pos].getId())
    {
        pos++;
    }

    std::swap(_produits[pos],_produits[_produits.size()-1]);
    _produits.pop_back();
}

int Magasin::nbLocations() const{
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient,int idProduit){
    _locations.push_back(Location{idClient,idProduit});
}

void Magasin::afficherLocations() const{
    std::cout<<"Liste des locations du magasin :"<<std::endl;
    for(const Location & l:_locations)
    {
        l.afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient,int idProduit){
    int pos=0;
    while(idClient != _locations[pos]._idClient && idProduit != _locations[pos]._idProduit)
    {
        pos++;
    }

    std::swap(_locations[pos],_locations[_locations.size()-1]);
    _locations.pop_back();
}
