#include "magasin.hpp"
#include <iostream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin) { };

TEST(GroupMagasin, Magasin_test1)  {
    Magasin mag;
    mag.ajouterClient("pierre");
    mag.ajouterClient("marie");
    mag.ajouterClient("jean");
    CHECK_EQUAL(3,mag.nbClients());
    //try{
    mag.supprimerClient(2);
    //}
    /*catch(std::string s){
        std::cerr<<"exception"<<s<<std::endl;
    }*/

    CHECK_EQUAL(2,mag.nbClients());

    mag.ajouterProduit("voiture");
    mag.ajouterProduit("camion");
    mag.ajouterProduit("moto");
    CHECK_EQUAL(3,mag.nbProduits());
    //try{
    mag.supprimerProduit(1);
    /*}
    catch(std::string s){
        std::cerr<<"exception"<<s<<std::endl;
    }*/

    CHECK_EQUAL(2,mag.nbProduits());
}
