#include "location.hpp"
#include "client.hpp"
#include "produit.hpp"
#include "magasin.hpp"
int main(){
    Location loc{1,2};
    loc.afficherLocation();

    Client cli (42,"toto");
    cli.afficherClient();

    Produit prod(3,"describe");
    prod.afficherProduit();

    Magasin mag ;
    mag.ajouterClient("jean");
    mag.ajouterClient("marie");
    mag.ajouterClient("pierre");
    mag.afficherClients();

    mag.ajouterProduit("voiture");
    mag.ajouterProduit("camion");
    mag.ajouterProduit("moto");
    mag.afficherProduits();


    return 0;
}
