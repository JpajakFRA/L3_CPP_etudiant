#include "client.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, Client_test1)  {
    Client cli(42,"toto");
    CHECK_EQUAL(42,cli.getId());
    CHECK_EQUAL("toto",cli.getNom());
}
