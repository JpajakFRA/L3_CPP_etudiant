#include <iostream>

#include "Liste.hpp"

int main() {

    Liste l1;
    l1.push_front(37);
    l1.push_front(13);
    Liste::iterator it=l1.begin();
    std::cout << *it << std::endl;

    return 0;
}

