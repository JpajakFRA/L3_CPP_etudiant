
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>
//TODO EN TEMPLATE ON PART AVEC UN TYPE T , ON REMPLACERA PARTOUT LE TYPE INT PAR LE TYPE T
// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
            int _valeur;
            Noeud* _ptrNoeudSuivant;
        };

        Noeud* _ptrTete;

    public:
        class iterator {
            private:
                Noeud* _ptrNoeudCourant;

            public:

                iterator(Noeud* ptrNoeudCourant):
                _ptrNoeudCourant(ptrNoeudCourant)
                {}

                const iterator & operator++() {
                   _ptrNoeudCourant=_ptrNoeudCourant->_ptrNoeudSuivant;
                   return *this;
                }

                int& operator*() const {
                    static int nimpe;
                    nimpe=_ptrNoeudCourant->_valeur;
                    return nimpe;
                }

                bool operator!=(const iterator & iter) const {

                    return _ptrNoeudCourant!=iter._ptrNoeudCourant;
                }

                friend Liste; 
        };

    public:
        Liste(){
            _ptrTete=nullptr;
        }

        void push_front(int val) {
            _ptrTete=new Noeud{val,_ptrTete};
        }

        int & front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
            Noeud* tmp=_ptrTete;
            while(_ptrTete){
                _ptrTete=_ptrTete->_ptrNoeudSuivant;
                delete tmp;
                tmp=_ptrTete;
            }
        }

        bool empty() const {
            if(_ptrTete==nullptr)
                return true;
            else
                return false;
        }

        iterator begin() const {
            return iterator(_ptrTete);
        }

        iterator end() const {
            return iterator(nullptr);
        }

        ~Liste(){
            this->clear();
        }

};

std::ostream& operator<<(std::ostream& os, const Liste & l) {
    for(Liste::iterator it=l.begin();it!=l.end();++it)
        os<<*it<<" ";
    return os;
}

#endif

