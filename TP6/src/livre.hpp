#ifndef LIVRE_HPP
#define LIVRE_HPP
#include <string>
#include <iostream>
using namespace std;

class Livre
{
private:
    string _titre;
    string _auteur;
    int _annee;
public:
    Livre();
    Livre(const string & titre,const string & auteur,int annee);
    const string & getTitre () const;
    const string & getAuteur () const;
    int getAnnee () const;
    friend ostream & operator << (ostream & os,const Livre & l);
    bool operator < (const Livre & l) const;
    bool operator == (const Livre & l) const;
};
ostream & operator << (ostream & os,const Livre & l);
#endif // LIVRE_HPP
