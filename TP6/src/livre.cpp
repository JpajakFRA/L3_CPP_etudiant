#include "livre.hpp"

Livre::Livre()
{
    _titre="";
    _auteur="";
    _annee=0;
}

Livre::Livre(const string & titre,const string & auteur,int annee):
    _annee(annee)
{
    bool a,b,c,d;
    if(titre.find(";") == std::string::npos)
        a=true;
    else{
       throw string("erreur : titre non valide (';' non autorisé)");
        a=false;
    }
    if(titre.find("\n") == std::string::npos)
        b=true;
    else{
        throw string("erreur : titre non valide ('\n' non autorisé)");
        b=false;
    }
    if(auteur.find(";") == std::string::npos)
        c=true;
    else{
       throw string("erreur : auteur non valide (';' non autorisé)");
        c=false;
    }
    if(auteur.find("\n") == std::string::npos)
        d=true;
    else{
       throw string("erreur : auteur non valide ('\n' non autorisé)");
        d=false;
    }

    if(a & b & c & d){
        _titre=titre;
        _auteur=auteur;
    }
}

const string & Livre:: getTitre () const {
    return _titre;
}

const string & Livre:: getAuteur () const {
    return _auteur;
}

int Livre:: getAnnee () const {
    return _annee;
}

ostream & operator << (ostream & os,const Livre & l){
    os<<l._titre<<";"<<l._auteur<<";"<<l._annee;
    return os;
}

 bool Livre:: operator < (const Livre & l) const{
     if(_auteur < l.getAuteur())
         return true;
     if(_auteur == l.getAuteur())
         if(_titre<l.getTitre())
             return true;
     return false;
 }

 bool Livre:: operator == (const Livre & l) const{
    if(_auteur == l.getAuteur() && _titre==l.getTitre() && _annee==l.getAnnee())
        return true;
    else
        return false;
 }
