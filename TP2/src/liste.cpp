#include "liste.hpp"

Liste::Liste(){
    tete=nullptr;
}

void Liste::ajouterDevant(int valeur){
    Noeud* ajout=new Noeud();
    ajout->valeur=valeur;
    ajout->suivant=tete;
    tete=ajout;
    //tete=new Noeud{n,tete};
}

int Liste::getTaille(){
    Noeud* courant=new Noeud();
    int cpt=0;
    if(tete==nullptr)
        return cpt;
    courant=tete;
    cpt+=1;
    while(courant->suivant!=nullptr)
    {
        cpt+=1;
        courant=courant->suivant;
    }
    return cpt;
}

int Liste::getElement(int indice){
    Noeud* courant=new Noeud();
    courant=tete;
    if(indice<0 || indice>getTaille())
    {
        return 0;
    }
    for(int i=0;i<indice;i++)
    {
        courant=courant->suivant;
    }
    return courant->valeur;
}


