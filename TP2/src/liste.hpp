#ifndef LISTE_HPP
#define LISTE_HPP

struct Noeud{
    int valeur;
    Noeud* suivant;
};

struct Liste{
   Noeud* tete;

   Liste();
   void ajouterDevant(int valeur);
   int getTaille();
   int getElement(int indice);
};
#endif // LISTE_HPP
