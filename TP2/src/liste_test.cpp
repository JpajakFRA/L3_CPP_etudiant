#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, liste_test1)  {
    Liste l;
    CHECK(l.tete==nullptr);
}

TEST(GroupListe, liste_test2) {
    Liste l1;
    l1.ajouterDevant(5);
    //check null tete et suivant
    CHECK(l1.tete->valeur==5);
}

TEST(GroupListe,Liste_test3) {
    Liste l2;
    l2.ajouterDevant(5);
    CHECK_EQUAL(l2.getTaille(),1);
    l2.ajouterDevant(4);
    CHECK_EQUAL(l2.getTaille(),2);
    l2.ajouterDevant(3);
    CHECK_EQUAL(l2.getTaille(),3);
}

TEST(GroupListe,Liste_test4) {
    Liste l3;
    l3.ajouterDevant(5);
    l3.ajouterDevant(2);
    l3.ajouterDevant(4);
    CHECK_EQUAL(l3.getElement(0),4);
    CHECK_EQUAL(l3.getElement(1),2);
    CHECK_EQUAL(l3.getElement(2),5);
    CHECK_EQUAL(l3.getElement(10),0);
}
