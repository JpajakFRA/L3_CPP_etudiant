#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));

}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}



std::stringstream Controleur:: getTexte(){
    std::stringstream ss;
    _inventaire.trier();
    ss<<_inventaire;
    return ss;
}


void Controleur::chargerInventaire(const std::string & nomFichier){
    std::ifstream ifs(nomFichier);
    if(ifs)
    {
        ifs>>_inventaire;
    }
    for(auto & v : _vues)
        v->actualiser();
}
