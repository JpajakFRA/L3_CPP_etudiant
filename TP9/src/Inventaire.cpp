#include "Inventaire.hpp"
#include <algorithm>


std::ostream& operator <<(std::ostream& os, const Inventaire & i) {
    for(auto x : i._bouteilles){
        os << x;
    }
    return os;
}

std::istream & operator>>(std::istream & is,Inventaire & i){
    Bouteille b;
    while(is>>b){
        i._bouteilles.push_back(b);
    }
    return is;
}

void Inventaire::trier(){

    auto trib=[](const Bouteille & a,const Bouteille & b){
        return a._nom<b._nom;
    };
    _bouteilles.sort(trib);
}

