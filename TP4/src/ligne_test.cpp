#include "ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Ligne_test1)  {
    Couleur c{1,2,3};
    Point p0{0,0},p1{100,200};
    Ligne l (c,p0,p1);

    Couleur c1=l.getCouleur();
    CHECK_EQUAL(c._r,c1._r);
    CHECK_EQUAL(c._g,c1._g);
    CHECK_EQUAL(c._b,c1._b);

    Point p0b=l.getP0();
    Point p1b=l.getP1();
    CHECK_EQUAL(p0._x,p0b._x);
    CHECK_EQUAL(p0._y,p0b._y);
    CHECK_EQUAL(p1._x,p1b._x);
    CHECK_EQUAL(p1._y,p1b._y);
}
