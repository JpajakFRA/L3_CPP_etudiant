#include<iostream>
#include"couleur.hpp"
#include"figuregeometrique.hpp"
#include"ligne.hpp"
#include"point.hpp"
#include"polygoneregulier.hpp"
#include <gtkmm.h>

int main(int argc,char** argv){
    /*Point p0{0,0},p1{100,200};
    Couleur c {1,0,0};
    Ligne l (c,p0,p1);
    l.afficher();
    PolygoneRegulier p (c,p1,50,5);
    p.afficher();
    */

    Gtk::Main kit(argc,argv);
    Gtk::Window window;
    window.show_all();
    kit.run(window);
    return 0;
}
