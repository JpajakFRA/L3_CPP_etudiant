#include "polygoneregulier.hpp"
#include <cmath>
#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur,const Point & centre,int rayon,int nbCotes):
    FigureGeometrique(couleur),_nbPoints(nbCotes){

    _points=new Point[_nbPoints];
    for(int i=0;i<_nbPoints;i++)
    {
        float tetai=(2*M_PI/(double)_nbPoints)*i;
        _points[i]._x=rayon*cos(tetai)+centre._x;
        _points[i]._y=rayon*sin(tetai)+centre._y;
    }
}

void PolygoneRegulier::afficher() const {
    std::cout<<"PolygoneRegulier "<<_couleur._r<<"_"<<_couleur._g<<"_"<<_couleur._b<<" ";
    for(int i=0;i<_nbPoints;i++)
    {
        std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
    }
    std::cout<<std::endl;
}
