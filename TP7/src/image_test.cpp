#include "image.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Image){
    Image i (10,10);
    CHECK_EQUAL(10,i.getLargeur());
    CHECK_EQUAL(10,i.getHauteur());
    //i.setPixel(5,6,255);
    //CHECK_EQUAL(255,i.getPixel(5,6));
    i.pixel(5,6)=255;
    int c=i.pixel(5,6);
    CHECK_EQUAL(255,c);
}
