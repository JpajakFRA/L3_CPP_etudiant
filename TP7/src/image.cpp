#include "image.hpp"

Image::Image(int largeur,int hauteur):
    _largeur(largeur),_hauteur(hauteur)
{
    _pixels=new int[_largeur*_hauteur];
}

int Image::getLargeur() const{
    return _largeur;
}

int Image::getHauteur() const{
    return _hauteur;
}

const int & Image::pixel(int i,int j) const {
    return _pixels[_largeur*i+j];
}

int & Image::pixel(int i,int j){
    return _pixels[_largeur*i+j];
}

Image::~Image(){
    delete [] _pixels;
}
