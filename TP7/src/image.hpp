#ifndef IMAGE_HPP
#define IMAGE_HPP
#include<string>
#include<iostream>
#include<fstream>
using namespace std;

class Image
{
private:
    int _largeur;
    int _hauteur;
    int* _pixels;
public:
    Image(int largeur,int hauteur);
    int getLargeur() const;
    int getHauteur() const;
    //int getPixel(int i,int j) const;
    //void setPixel(int i, int j,int couleur);
    const int & pixel(int i,int j) const;
    int & pixel(int i,int j);
    ~Image();
};

#endif // IMAGE_HPP


