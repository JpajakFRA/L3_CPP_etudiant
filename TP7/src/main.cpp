#include "image.hpp"
#include<string>
#include<iostream>
#include<fstream>
#include <cmath>
using namespace std;

void ecrirePnm(const Image & img,const string & nomFichier){
    ofstream fichier(nomFichier);
    if(fichier){
        fichier<<"P2"<<endl;
        fichier<<img.getLargeur()<<" "<<img.getHauteur()<<endl;
        fichier<<"255"<<endl;
        for(int i=0;i<img.getLargeur();i++){
            for(int j=0;j<img.getHauteur();j++){
                fichier<<img.pixel(i,j)<<" ";
            }
            fichier<<endl;
        }
    }
    else
        cout<<"erreur ouverture"<<endl;
}

void remplir(Image & img){
    for(int i=0;i<img.getLargeur();i++){
        for(int j=0;j<img.getHauteur();j++){
            img.pixel(i,j)=255; //(1+cos((2*M_PI*i/img.getLargeur())*)*127);
        }
    }
}

int main(){
    Image i (50,50);
    remplir(i);
    ecrirePnm(i,"test.pnm");
    return 0;
}
