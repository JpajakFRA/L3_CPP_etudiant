#include <iostream>
#include "Fibonacci.hpp"
#include "Vecteur3.hpp"

using namespace std;

int main(){
	
	int terme7=7;
	int fib=fibonacciIteratif(terme7);
	cout<<"Voici le terme "<<terme7<<"de la suite de Fibonacci iteratif: "<<fib<<endl;
	int fibrec=fibonacciRecursif(terme7);
	cout<<"Voici le terme "<<terme7<<"de la suite de Fibonacci recursif : "<<fibrec<<endl;
	Vecteur3D v1{2,3,6};
	
	return 0;
}
