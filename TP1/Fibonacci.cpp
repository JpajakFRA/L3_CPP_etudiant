#include <iostream>

using namespace std;

int fibonacciRecursif(int n){
	if(n < 2)
	{
	  return n;
	}
	else
	{
	  return fibonacciRecursif(n-1) + fibonacciRecursif(n-2);
	}
	return n;
}


int fibonacciIteratif(int n){
  int pre = 0;
  int courant = 1;
  int i, suivant;

  for(i = 2; i <= n; i++) 
  {
    suivant = pre+ courant;
    pre = courant;
    courant = suivant;
  }
  return courant;
}
