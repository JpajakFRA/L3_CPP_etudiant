#ifndef FIB_HPP_
#define FIB_HPP_

int fibonacciRecursif(int n);

int fibonacciIteratif(int n);

#endif
