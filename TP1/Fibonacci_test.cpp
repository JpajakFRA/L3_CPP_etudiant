#include "Fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci){};
TEST(GroupFibonacci,test_fibonnaci_rec){
    CHECK_EQUAL(fibonacciRecursif(1),1);
    CHECK_EQUAL(fibonacciRecursif(2),1);
    CHECK_EQUAL(fibonacciRecursif(3),2);
    CHECK_EQUAL(fibonacciRecursif(4),3);
    CHECK_EQUAL(fibonacciRecursif(5),5);
}

TEST(GroupFibonacci, test_fibonnaci_ite){
    CHECK_EQUAL(fibonacciIteratif(1),1);
    CHECK_EQUAL(fibonacciIteratif(2),1);
    CHECK_EQUAL(fibonacciIteratif(3),2);
    CHECK_EQUAL(fibonacciIteratif(4),3);
    CHECK_EQUAL(fibonacciIteratif(5),5);
}
