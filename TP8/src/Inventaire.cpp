#include "Inventaire.hpp"



std::ostream& operator <<(std::ostream& os, const Inventaire & i) {
    for(const Bouteille & b : i._bouteilles){
        os << b;
    }
    return os;
}

std::istream & operator>>(std::istream & is,Inventaire & i){
    Bouteille b;
    while(is){
        is>>b;
        i._bouteilles.push_back(b);
    }
    return is;
}
