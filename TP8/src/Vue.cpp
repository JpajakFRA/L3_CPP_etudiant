#include "Controleur.hpp"
#include "Vue.hpp"

#include <iostream>
#include <fstream>
#include <string>
///////////////////////////////////////
// Vue
///////////////////////////////////////

Vue::Vue(Controleur& controleur) : _controleur(controleur) {}

///////////////////////////////////////
// VueGraphique
///////////////////////////////////////

VueGraphique::VueGraphique(int argc, char ** argv, Controleur & controleur) :
Vue(controleur), _kit(argc, argv) {

	// la fenêtre principale
	_window.set_title("Viewer de bouteilles");
	_window.set_size_request(400, 300);
	_window.set_resizable(false);
	_window.set_border_width(5);
    _button.set_label("Charger un fichier de bouteille");
	// ScrolledWindow : la fenêtre avec scrolling contenant le TextView
	_scrolledWindow.add(_textView);
	_scrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	_box.pack_start(_scrolledWindow);
    _box.add(_button);
    _button.signal_clicked().connect(sigc::mem_fun(*this,&VueGraphique::ouvrirFichier));
	_window.add(_box);
	_window.show_all();
}

void VueGraphique::actualiser() {
    std::stringstream ss=_controleur.getTexte();
    std::string texte =ss.str();
	_textView.get_buffer()->set_text(texte.c_str());
}

void VueGraphique::run() {
	_kit.run(_window);
}

void VueGraphique::ouvrirFichier() {
	Gtk::FileChooserDialog dialog(_window, "Ouvrir fichier...");
	dialog.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	dialog.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
	int ret = dialog.run();
	if (ret == Gtk::RESPONSE_OK) {
        std::string nomFichier = dialog.get_filename();//
        _controleur.chargerInventaire(nomFichier);
        /*std::ifstream is (nomFichier);
        Bouteille b;
        while(is){
            is>>b;
            //std::cout<<b<<std::endl;
            _controleur._inventaire._bouteilles.push_back(b);
        }*/
	}
}
VueConsole::VueConsole(Controleur & controleur) :
    Vue(controleur)
{

}

void VueConsole::actualiser(){
    std::stringstream ss=_controleur.getTexte();
    std::string texte =ss.str();
    std::cout<<texte<<std::endl;
}

void VueConsole::run(){

}

