#include "zonedessin.hpp"
#include "ligne.hpp"
#include "couleur.hpp"
#include "polygoneregulier.hpp"

ZoneDessin::ZoneDessin(){
    Point p0{0,0},p1{100,200};
    Couleur c {1,0,0};
    Ligne* l=new Ligne(c,p0,p1);
    PolygoneRegulier* p=new PolygoneRegulier(c,p1,50,5);
    _figures.push_back(l);
    _figures.push_back(p);
}


ZoneDessin::~ZoneDessin(){
    for(FigureGeometrique* f:_figures)
        delete f;
}
bool ZoneDessin:: on_draw(const Cairo::RefPtr<Cairo::Context> & contexte){
    contexte->set_source_rgb(1.0,0.0,0.0);
    contexte->set_line_width(5.0);
    for(FigureGeometrique* f:_figures)
        f->afficher();
    contexte->stroke();
    return true;
}
//bool ZoneDessin:: gererClic(GdkEventButton* event){}
//gererclic pour connecter signaux souris
