#include<iostream>
#include "couleur.hpp"
#include "figuregeometrique.hpp"
#include "ligne.hpp"
#include "point.hpp"
#include "polygoneregulier.hpp"
#include <gtkmm.h>
#include "viewerfigures.hpp"
#include "zonedessin.hpp"

int main(int argc, char ** argv) {

    ViewerFigures V (argc,argv);
    V.run();




    /*Point p0{0,0},p1{100,200};
    Couleur c {1,0,0};
    Ligne l (c,p0,p1);
    l.afficher();
    PolygoneRegulier p (c,p1,50,5);
    p.afficher();
    */

    return 0;
}
