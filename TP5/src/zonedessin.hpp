#ifndef ZONEDESSIN_HPP
#define ZONEDESSIN_HPP
#include <vector>
#include "figuregeometrique.hpp"
#include<gtkmm.h>

class ZoneDessin : public Gtk::DrawingArea
{
private:
std::vector<FigureGeometrique*> _figures;

public:
ZoneDessin();
~ZoneDessin();
protected:
bool on_draw(const Cairo::RefPtr<Cairo::Context> & contexte);
//bool gererClic(GdkEventButton* event);
};

#endif // ZONEDESSIN_HPP
